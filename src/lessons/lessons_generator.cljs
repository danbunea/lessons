(ns lessons.lessons-generator)

(defn generate-choice [operation entries result]
  {
    :operation operation
    :entries entries
    :result (apply operation entries)})

(defn generate-choices [question]
  (let [
         operation (:operation question)
         entries (:entries question)
         a (-> entries first)
         b (-> entries second)
         result (apply operation entries)
        ]
  (->>
  [
    (generate-choice operation entries result)
    (generate-choice operation [a (inc b)] (inc result))
    (generate-choice operation [(dec a) b] (dec result))
    (generate-choice operation [a (dec b)] result)
    ]
    shuffle
    (map-indexed (fn [index choice]
                   [(keyword (str (inc index))) choice]))
    (into {})
    )

    ))



(defn question [operation & entries]
  (let [question-entries (into [] entries)
        question {
                  :operation operation
                  :entries  question-entries
                  }
        choices (generate-choices question)]
    {
      :question question
      :choices choices
      }
    ))
