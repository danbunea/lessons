(ns lessons.core
    (:require
      [reagent.core :as reagent :refer [atom]]
      [lessons.lessons-model :as m :refer [model]]
      [lessons.lessons-controller :as controller]
      [lessons.lessons-views :as views :refer [screen-component]]
              ))

(enable-console-print!)

(println "This text is printed from src/cljs-kata/core.cljs. Go ahead and edit it and see reloading in action.")


(defonce init
  (do
    (print "initializing..." )
    (controller/init )
    (print "initialized")))

(defn application []
  [screen-component @model])

(reagent/render-component [application]
                          (. js/document (getElementById "app")))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
