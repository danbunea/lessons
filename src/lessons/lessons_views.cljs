(ns lessons.lessons-views
  (:require
    [reagent.core :as r]
    [lessons.lessons-controller :as controller]
    ))


(defn dots-component [how-many]
  [:div.dots

   (for [dot (range how-many)]
     ^{:key (str dot)}
     [:span.dot
      ])
   ])


(defn choice-component [choice-key choice]
        (let [
               a (-> choice :entries first)
               b (-> choice :entries second)
               ]

          [:div.choice
           {
             :id (str "choice-" (name choice-key))
             :on-click #(controller/reply choice-key)
             }
           ;;              [:div.choiche-id (str (name choice-key) ". ")]
           [:div.a a]
           [:div.operation (when (= + (:operation choice)) "+")]
           [:div.b b]
           [:div.equals "="]
           [:div.result (:result choice)]
           ]))

(defn add-dots-component [a b ]
  [:div
   [:div.a
    [dots-component a]

    ]
   [:div.operation "+"]
   [:div.b
    [dots-component b]]]
  )

;; (defn subtract-dots-component [a b]
;;   [:div.substract
;;    [:div.substract-left
;;     [:div.a
;;      [dots-component a]]
;;     [:div.operation "-"]]
;;    [:div.substract-right
;;     [:div.b
;;      [dots-component b]]]
;;    ]
;;   )


(defn exercise-component [exercise]
  (let [
         question (:question exercise)
         a (-> question :entries first)
         b (-> question :entries second)
         ]
    [:div
     [:div.exercise
      (when (= + (:operation question))
        [add-dots-component a b]
        )
      [:div.equals "="]
      [:div.question-mark "?"]]
     [:div.choices
      (for [[choice-key choice] (:choices exercise)]
          ^{:key (str (:entries choice))}
          [choice-component choice-key choice]
        )
      ]]))


(defn lesson-component [state]
  (let [
         selected (-> state :selected :exercise)
         exercises (get-in state [:exercises :math-add-0-to-9])
         exercise (get exercises selected)
         ]
    [:div.lesson
     [:div.score (str (name selected) " / " (count exercises))]
     [exercise-component exercise]
     ])
  )


(defn result-component [result]
  [:div.final-result result]
  )



(defn screen-component [state]
  (if-let [result (-> state :exercises :math-add-0-to-9 :results)]
    [result-component result]
    ;;else
    [lesson-component state])
  )
