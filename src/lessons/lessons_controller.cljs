(ns lessons.lessons-controller
  (:require
    [lessons.lessons-model :refer [model result]]
    [lessons.lessons-generator :refer [question]]
    [lessons.http :refer [save]]
    [cljs.pprint :refer [pprint]]))


(defn swapm! [x y]
            (swap! y (fn [xx] x))
  )


(defn generate-exercise [no]
    (question + (inc (rand-int 10)) (inc (rand-int 3))))


(defn init []
  (print 'init)
  (let [
         exercises (->> (range 10)
                        (map #(generate-exercise %) )
                        (map-indexed (fn [index exercise]
                                       [(keyword (str (inc index))) exercise]))
                        (into {}))
         ]
    (-> @model
        (assoc-in [:exercises :math-add-0-to-9] exercises)
        (assoc-in [:selected :exercise] :1)
        (swapm! model)
        )))




(defn reply [answer]
  (print 'reply answer)
  (let [
         selected (-> @model :selected :exercise)
         current (get-in @model [:exercises :math-add-0-to-9 selected])
         question (:question current)
         choice (get-in current [:choices answer])
         ok? (=
               (conj (:entries question) (apply (:operation question) (:entries question)))
               (conj (:entries choice) (:result choice)))
         next-val (inc (int (name selected)))
         following (keyword (str next-val))
         ]

    (-> @model
        (assoc-in [:exercises :math-add-0-to-9 selected :ok] ok?)
        (assoc-in [:exercises :math-add-0-to-9 selected :reply] answer)
        (#(if (> next-val (count (get-in @model [:exercises :math-add-0-to-9])))
            (let [score (assoc-in %
                                  [:exercises :math-add-0-to-9 :results]
                                  (result (-> % :exercises :math-add-0-to-9 vals)))]
              (save %)
              score)

            ;;else
            (assoc-in % [:selected :exercise] following)
            ))

        (swapm! model))
    ))


