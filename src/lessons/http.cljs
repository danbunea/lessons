(ns lessons.http
  (:require  [ajax.core :refer [POST ajax-request json-request-format json-response-format]])
  )


(defn handler2 [[ok response]]
  (if ok
    (.log js/console (str response))
    (.error js/console (str response))))

(defn save [data]
(ajax-request
        {:uri "save.php"
         :method :post
         :params {:data data}
         :handler handler2
         :format (json-request-format)
         :response-format (json-response-format {:keywords? true})}))
