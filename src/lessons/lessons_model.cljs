(ns lessons.lessons-model
    (:require [reagent.core :as r]))




(def initial-model {
                      :exercises {:math-add-0-to-9 {}}})


(defonce model (r/atom initial-model))


(defn result [exercises]
  (print 3 (map :ok exercises))
  (->> exercises
    (map (fn [exercise]
           (if (:ok exercise)
             1
             0)))
    (reduce + 0)
    ))


(defn ^:export jsmodel []
  (clj->js @model))
