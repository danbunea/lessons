(ns lessons.lessons-generator-should
  (:require
    [cljs.test :refer-macros [deftest testing is are]]
    [lessons.lessons-generator :refer [question]]
    [cljs.pprint :refer [pprint]]
    [lessons.lessons-test-data :as data]
    )
  )


(defn res [o]
  (select-keys o [:result :entries]))


(deftest should-build-an-excercise
  (let [expected data/default-exercise
        exercise (question + 5 3)
        ]


    (is (= (:question expected) (:question exercise)))
    (is (= (count (:choices expected)) (count (:choices exercise))))
    (is (= 1 (->> (:choices exercise)
                  vals
                  (filter #(=
                             (apply (-> expected :question :operation) (-> expected :question :entries))
                             (:result %)))
                  count
                  )))

    (is (not (nil? (some #{(-> exercise :choices :1)} (-> exercise :choices vals)))))
    (is (not (nil? (some #{(-> exercise :choices :2)} (-> exercise :choices vals)))))
    (is (not (nil? (some #{(-> exercise :choices :3)} (-> exercise :choices vals)))))
    (is (not (nil? (some #{(-> exercise :choices :4)} (-> exercise :choices vals)))))
    ))

