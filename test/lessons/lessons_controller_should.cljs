(ns lessons.lessons-controller-should
  (:require
    [lessons.lessons-controller :as controller]
    [lessons.lessons-model :refer [model]]
    [lessons.http :refer [save]]
    [cljs.pprint :refer [pprint]]
    [lessons.test-utils :refer [==?]]
    [cljs.test :refer-macros [deftest testing is are use-fixtures]]
    ))



(use-fixtures :each
  {:before (fn [] (reset! model {}))})



(deftest should-start-a-lesson-with-10-exercises
  (let [after-model (controller/init )
        lesson (-> after-model :exercises :math-add-0-to-9)
        exercise-one (-> lesson :1)]
    (==? 10 (-> after-model :exercises :math-add-0-to-9 count))
    (==? nil (:ok exercise-one))
    (==? + (-> exercise-one :question :operation))
    (==? :1 (-> after-model :selected :exercise))
    )
  )



(deftest should-move-to-the-next-exercise
  (let [initial-model (controller/init )
        lesson (-> initial-model :exercises :math-add-0-to-9)
        exercise-two (:2 lesson)
        ]

    (controller/reply :1)
    (is (not (nil? (-> initial-model :exercises :math-add-0-to-9 :1))))
    (==? :2 (-> @model :selected :exercise))
    )
  )



(deftest should-finish-the-lesson
  (with-redefs [save #(identity %)]
    (let [initial-model (controller/init )
          lesson (-> initial-model :exercises :math-add-0-to-9)
          exercise-two (:2 lesson)
          ]

      (controller/reply :1)
      (controller/reply :2)
      (controller/reply :3)
      (controller/reply :4)
      (controller/reply :1)
      (controller/reply :2)
      (controller/reply :3)
      (controller/reply :4)
      (controller/reply :1)
      (controller/reply :2)
      (is (not (nil? (-> @model :exercises :math-add-0-to-9 :results))))
      )
    )
  )





