(ns lessons.lessons-model-should
  (:require
    [cljs.test :refer-macros [deftest testing is are use-fixtures]]
    [lessons.lessons-model :refer [model result]]
    [cljs.pprint :refer [pprint]]
    [lessons.test-utils :refer [==?]]
    ))


(deftest compute-the-result
  (==? 1 (result [
                   {:ok false}
                   {:ok true}
                   {:ok false}
                   ])))

