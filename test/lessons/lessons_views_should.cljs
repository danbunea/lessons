(ns lessons.lessons-views-should
  (:require
    [cljs.test :refer-macros [deftest testing is are]]
    [lessons.lessons-views :as views]
    [lessons.lessons-controller :as controller]
    [cljs.pprint :refer [pprint]]
    [reagent.core :as r :refer [atom]]
    [cljsjs.enzyme]
    [lessons.lessons-test-data :as data]
    [lessons.test-utils :refer [==?]]
    )
  )



(deftest render-an-exercise-again []
  (let [exercise data/default-exercise
        component [views/exercise-component exercise]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))
        ]
    (==? (-> exercise
               :question
               :entries
               first)
           (-> mounted
               (.find  ".a .dots .dot")
               (.-length)))
    (==? (-> exercise
               :question
               :entries
               second)
           (-> mounted
               (.find  ".b .dots .dot")
               (.-length)))

    (==? (-> exercise
               :choices
               count)
           (-> mounted
               (.find  ".choices .choice")
               (.-length)))



    ))

(deftest should-render-a-choice
  (let [choice-key :1
        choice (:1 data/default-choices)
        component [views/choice-component choice-key choice]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))
        ]
    (==? (-> choice
               :entries
               first)
           (-> mounted
               (.find  ".a")
               .props
               .-children
               ))
    (==? (-> choice
               :entries
               second)
           (-> mounted
               (.find  ".b")
               .props
               .-children
               ))
    (==? (-> choice
               :result
               )
           (-> mounted
               (.find  ".result")
               .props
               .-children
               ))

    ))



(deftest should-invoke-reply-when-clicked
  (let [choice-key :3
        choice (:3 data/default-choices)
        component [views/choice-component choice-key choice]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))
        invocations (atom [])
        ]
    (with-redefs [controller/reply #(swap! invocations conj %)]

           (-> mounted
               (.find  ".result")
               (.simulate "click")
               )

    (==? [choice-key] @invocations)))

    )





(deftest should-render-the-result-on-screen-when-a-result-exists
  (let [
        component [views/screen-component (assoc-in data/default-model [:exercises :math-add-0-to-9 :results] 5)]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))
        ]
    (==? 5
           (-> mounted
               (.find  ".final-result")
               .props
               .-children
               ))))





(deftest should-render-the-selected-lesson-when-not-finishe
  (let [
        component [views/screen-component data/default-model]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))
        ]
    (==? 1
           (-> mounted
               (.find  ".exercise")
               .-length
               ))))
