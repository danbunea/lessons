(ns lessons.lessons-test-data
  )

(def default-choices {
                             :1 {
                                :operation +
                                :entries [5 3]
                                :result 8}
                             :2 {
                                :operation +
                                :entries [5 2]
                                :result 8}
                             :3 {
                                :operation +
                                :entries [4 3]
                                :result 7}
                             :4 {
                                :operation +
                                :entries [5 4]
                                :result 9}
                              })


(def default-exercise {
                   :question {
                               :operation +
                               :entries [5 3]}
                   :choices default-choices
                   })


(def default-model {
                     :exercises {
                                  :math-add-0-to-9 {
                                                     :1 default-exercise}
                                  }

                     :selected {:exercise :1}
                     })

